# Symdep

A python package manager with centralized artifact storage
Ever wanted to centralize dependencies so that you download them once instead of a per-virtualenv basis?
Then Symdep is for you! This project aims to be a full-fledged package manager with a simple CLI.

# WIP

This project is heavely WIP, you shouldn't use it for now.
But you can use [poetry](https://python-poetry.org/), as Symdep uses `pyproject.toml` to store its data too.


## Installation

Use the [pip](https://pip.pypa.io/en/stable/) package manager to install Symdep.

```bash
$ pip install symdep
```

## Usage

```bash
$ symdep init

$ symdep add requests
$ symdep add mypy -D
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)