import os
import subprocess
import sys

environ = {
	**os.environ,
	'PYTHONPATH': 'C:/Users/Michele/PycharmProjects/Symdep/src',
	'SYMDEP_FOLDER_OVERRIDE': 'C:/Users/Michele/PycharmProjects/Symdep/test/data'
}


while True:
	proc = subprocess.run(
		executable=sys.executable,
		args=f'-d -m symdep { input( "> " ) }',
		stderr=sys.stderr,
		stdout=sys.stdout,
		stdin=sys.stdin,
		env=environ,
		cwd='C:/Users/Michele/PycharmProjects/Symdep/test'
	)
	sys.stderr.flush()
	sys.stdout.flush()
	print( 'exit code:', proc.returncode )

