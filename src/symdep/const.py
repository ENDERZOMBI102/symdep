from typing import Final


VERSION: Final[str] = '0.1.0'
COMMIT_HASH: Final[str] = '$NO_HASH$'
