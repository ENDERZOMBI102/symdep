import os
import sys
from pathlib import Path


WINDOWS = sys.platform.startswith('win') or ( sys.platform == 'cli' and os.name == 'nt' )


def symdepFolder() -> Path:
	""" Returns the symdep folder path """

	if ( override := os.getenv('SYMDEP_FOLDER_OVERRIDE') ) is not None:  # make the data dir overridable
		return Path( override )
	elif WINDOWS:
		return Path( os.path.expandvars('%LOCALAPPDATA%') ) / 'Symdep'
	elif sys.platform == 'darwin':
		raise NotImplementedError('Getting the symdep local folder is not supported for darwin-based systems.')
	else:
		return Path( '~/.Symdep' ).expanduser()


def currentWorkDir() -> Path:
	return Path( os.getcwd() )
