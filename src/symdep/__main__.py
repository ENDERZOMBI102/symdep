import sys

from clikit.args import ArgvArgs

import symdep.cli.application


symdep.cli.application.SymdepApp().run( ArgvArgs( [ *sys.argv, '--ansi' ] ) )
