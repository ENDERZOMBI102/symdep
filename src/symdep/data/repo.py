import os
from pathlib import Path

from symdep.data.package import Package


class DirRepo:
	""" A local repository with maven structure """
	_path: Path

	def __init__( self, path: Path ) -> None:
		self._path = path

	def __contains__(self, item: Package ) -> bool:
		if ( pkgDir := self._path / item.name ).exists():
			os.listdir( pkgDir )

		return False
