from pathlib import Path
from typing import Iterator

import toml

from symdep.data.package import Package


class PackageList:
	_packages: list[ Package ]

	def __init__( self ) -> None:
		self._packages = []

	def add( self, package: Package = None, name: str = None, version: str = None ):
		if package is None:
			package = Package( name, version )

		self._packages.append( package )

	def __iter__(self) -> Iterator[Package]:
		return self._packages.__iter__()


class Project:
	name: str
	version: str
	description: str
	authors: list[ str ]
	license: str

	dependencies: PackageList
	devDependencies: PackageList

	def __init__( self, projDir: Path ) -> None:
		pyproject = toml.loads( ( projDir / 'pyproject.toml' ).read_text() )

		self.dependencies = PackageList()
		for package, version in pyproject['tool']['symdep']['dependencies'].items():
			self.dependencies.add( name=package, version=version )

		self.devDependencies = PackageList()
		for package, version in pyproject['tool']['symdep']['dev-dependencies'].items():
			self.devDependencies.add( name=package, version=version )

	def isInstalled( self, package: Package, dev: bool = False ) -> bool:
		if dev:
			return package in self.devDependencies
		else:
			return package in self.devDependencies
