from dataclasses import dataclass


@dataclass
class Package:
	name: str
	version: str

	@classmethod
	def parse( cls, string: str ) -> 'Package':
		# mypy>=0.931
		#      ^
		pkgname, version = '', None
		if '=' in string:
			for index, char in enumerate( string ):
				if char not in '<=>':
					pkgname += char
				else:
					version = string[index:]
					break
		else:
			pkgname = string

		return Package( name=pkgname, version=version )

	@classmethod
	def parseList( cls, rawPackages: list[ str ] ) -> list[ 'Package' ]:
		return list( map( cls.parse, rawPackages ) )
