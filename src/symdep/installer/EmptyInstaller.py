from symdep.data import Package
from symdep.installer import Installer


class EmptyInstaller( Installer ):
	""" Installer that does nothing """

	def install( self, package: Package ) -> None: pass

	def update( self, package: Package ) -> None: pass

	def remove( self, package: Package ) -> None: pass
