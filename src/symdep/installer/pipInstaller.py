from symdep.data import Package
from symdep.installer import Installer


class PipInstaller( Installer ):
	""" Installer that installs packages using pip """

	def install( self, package: Package ) -> None:
		pass

	def update( self, package: Package ) -> None:
		pass

	def remove( self, package: Package ) -> None:
		pass
