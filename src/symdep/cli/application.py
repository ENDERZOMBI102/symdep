import os

from cleo import Application

from symdep import const, util
from symdep.cli.command.AddCommand import AddCommand
from symdep.cli.command.RemoveCommand import RemoveCommand
from symdep.cli.command.InstallCommand import InstallCommand
from symdep.cli.command.UpdateCommand import UpdateCommand
from symdep.cli.command.self import SelfCommand
from symdep.cli.command.InfoCommand import InfoCommand
from symdep.cli.command.InitCommand import InitCommand
from symdep.cli.command.WhereCommand import WhereCommand
from symdep.cli.command.ShellCommand import ShellCommand
from symdep.cli.command.LockCommand import LockCommand
from symdep.data.repo import DirRepo

from symdep.data.project import Project


class SymdepApp( Application ):
	repo: DirRepo
	current: Project

	def __init__( self ):
		super().__init__(
			name='Symdep',
			version=const.VERSION
		)
		self.add_commands(
			AddCommand(),
			RemoveCommand(),
			InstallCommand(),
			UpdateCommand(),
			SelfCommand(),
			InfoCommand(),
			InitCommand(),
			WhereCommand(),
			ShellCommand(),
			LockCommand()
		)
		self._repo = DirRepo( util.symdepFolder() / 'repo' )
		self.current = Project( util.currentWorkDir() )

