from typing import Optional, TYPE_CHECKING

from cleo import Command

from symdep.data.project import PackageList

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class InfoCommand( Command ):
	"""
		Shows info about the project or a package

		info
			{package? : Package to show}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:

		def showPackages( packages: PackageList ) -> None:
			for package in packages:
				self.line( f'\t- {package.name}  {package.version}' )

		self.line('Packages:')
		showPackages( self.application.current.dependencies )

		self.line( '\nDev Packages:' )
		showPackages( self.application.current.dependencies )

		return 0
