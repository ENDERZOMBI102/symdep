from typing import Optional, TYPE_CHECKING

from cleo import Command

from symdep.data.package import Package

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class AddCommand(Command):
	"""
		Adds a package to dependencies or development dependencies

		add
			{packages* : Packages to install}
			{--D|dev : If set, will add the packages to dev dependencies}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[int]:
		toAdd = self.argument( 'packages' )
		self.line(f'Installing {len( toAdd )} packages..')

		for package in Package.parseList( toAdd ):
			self.write( f'\t Installing {package.name}..' )
			if self.application.current.isInstalled( package ):
				self.overwrite( f'\t Installing {package.name}..  Already Installed, Skipping.' )
				continue



		return None
