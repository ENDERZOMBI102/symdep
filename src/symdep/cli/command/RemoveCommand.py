from typing import Optional, TYPE_CHECKING

from cleo import Command

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class RemoveCommand( Command ):
	"""
		Remove a package from dependencies or development dependencies

		remove
			{package* : Packages to remove}
			{--D|dev : If set, will remove the packages from dev dependencies}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		print( self.__class__.__name__ )
		return 0
