from typing import Optional

from cleo import Command


class InitCommand( Command ):
	"""
		Creates a new project,

		init
			{--git : If set, will initialize an empty git repo}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		print( self.__class__.__name__ )
		return 0
