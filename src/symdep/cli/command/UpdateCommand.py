from typing import Optional, TYPE_CHECKING

from cleo import Command

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class UpdateCommand( Command ):
	"""
		Update all packages to latest possible version ( indicated by pyproject.toml )

		update
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		print( self.__class__.__name__ )
		return 0
