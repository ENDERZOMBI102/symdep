from typing import Optional, TYPE_CHECKING

from cleo import Command

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class WhereCommand( Command ):
	"""
		Shows the path of a package

		where
			{package : Packages to show}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		print( self.__class__.__name__ )
		return 0
