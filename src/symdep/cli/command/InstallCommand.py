from typing import Optional, TYPE_CHECKING

from cleo import Command

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class InstallCommand( Command ):
	"""
		Creates a new venv if missing and installs all non-dev dependencies

		install
			{--D|dev : If set, will install dev dependencies too}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		print( self.__class__.__name__ )
		return 0
