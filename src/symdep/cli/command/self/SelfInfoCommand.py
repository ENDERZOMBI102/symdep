from typing import Optional, TYPE_CHECKING

from cleo import Command

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class SelfInfoCommand( Command ):
	"""
		Info about Symdep

		info
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		self.write(
			f'<fg=green>Symdep python package manager by ENDERZOMBI102\n'
			f'A package manager with space constraints in mind</>\n'
			f'<fg=cyan>Version </>{const.VERSION}\n'
			f'<fg=cyan>Commit hash </>{const.COMMIT_HASH}'
		)
		return 0
