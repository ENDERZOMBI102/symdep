from typing import Optional, TYPE_CHECKING

from cleo import Command

if TYPE_CHECKING:
	from symdep.cli.application import SymdepApp


class SelfConfigCommand( Command ):
	"""
		Get or set a config value

		config
			{config : Config to show or set}
	"""
	application: 'SymdepApp'

	def handle( self ) -> Optional[ int ]:
		print( self.__class__.__name__ )
		return 0
