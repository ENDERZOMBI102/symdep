from typing import Optional

from cleo import Command

from symdep.cli.command.self.SelfConfigCommand import SelfConfigCommand
from symdep.cli.command.self.SelfInfoCommand import SelfInfoCommand
from symdep.cli.command.self.SelfUpdateCommand import SelfUpdateCommand


class SelfCommand( Command ):
	"""
		Execute stuff on Symdep

		self
	"""

	commands = [
		SelfInfoCommand(),
		SelfUpdateCommand(),
		SelfConfigCommand()
	]

	def handle( self ) -> Optional[ int ]:
		return self.call( 'help', self._config.name )
